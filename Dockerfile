FROM base/archlinux
MAINTAINER Aaron Krill <aaron@krillr.com>

### Grab the latest mirror list
RUN curl -o /etc/pacman.d/mirrorlist "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&ip_version=6&use_mirror_status=on" && \
  sed -i 's/^#//' /etc/pacman.d/mirrorlist

### Update system
RUN pacman -Sy archlinux-keyring --noconfirm && \
    pacman-key --init && \
    pacman-key --populate archlinux && \
    pacman -Sy --noprogressbar --noconfirm && \
    pacman -S --force openssl --noconfirm && \
    pacman -S pacman --noprogressbar --noconfirm && \
    pacman-db-upgrade && \
    pacman -Syyu base base-devel net-tools openssl openssh pkgfile unzip unrar p7zip git wpa_supplicant networkmanager --noprogressbar --noconfirm && \
    rm -rf /var/cache/pacman/pkg/*.xz

### Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

### Fix broken perl paths
ENV PATH $PATH:/usr/bin/core_perl

### Set up passwordless sudo for wheel
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

